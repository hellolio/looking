# -*- coding: utf-8 -*-
# Date：         2019/9/9  下午9:33
# Description：

import functools
import threading

from api_db.logging_config import logger

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship
from sqlalchemy.engine.base import Engine, Connection


prod_database = {
    "host": "",
    "port": 3306,
    "user": "",
    "password": "",
    "db": "",
    "timeout": 60,
    "pool_size": 5,
}
local_database = {
    "host": "127.0.0.1",
    "port": 3306,
    "user": "root",
    "password": "hello",
    "db": "test",
    "timeout": 60,
    "pool_size": 5,
}


class DBConnect(object):
    __instance = False
    rlock = threading.RLock()

    def __new__(cls, *args, **kwargs):
        with DBConnect.rlock:
            if cls.__instance:
                return cls.__instance
            cls.__instance = super(DBConnect, cls).__new__(cls, )
            return cls.__instance

    def __init__(self, config):
        self.db_config = config
        try:
            self.ENGINE = create_engine(
                f'mysql+pymysql://{self.db_config.get("user")}:{self.db_config.get("password")}'
                f'@{self.db_config.get("host")}:{self.db_config.get("port")}/'
                f'{self.db_config.get("db")}?charset=utf8',
                max_overflow=0, pool_size=self.db_config.get('pool_size'))
        except Exception as e:
            logger.ERROR.error(f"建立数据库链接失败, {e}")

    def create_db(self):
        # 创建表
        Base = declarative_base()
        Base.metadata.create_all(self.ENGINE)

    def get_session(self):
        session = sessionmaker(bind=self.ENGINE)
        return session


db = DBConnect(local_database)


