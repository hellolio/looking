import base64

# 需要加密的文档
def readFile(filename):
    with open(filename, "rb") as f:
        text = f.read()
    return text

# 生成加密的文档
def writeFile(filename,text,enOrDe):
    if enOrDe == "en":
        filename = "encry_" + filename
    elif enOrDe == "de":
        filename = "decry_" + filename

    with open(filename, "wb") as f:
        f.write(text)

# 加密
def encryption(filename):
    text = readFile(filename)
    base64_text = base64.b64encode(text)
    writeFile(filename,base64_text,"en")


# 解密
def decryption(filename):
    base64_text = readFile(filename)
    text = base64.b64decode(base64_text)
    writeFile(filename,text,"de")


if __name__=="__main__":
    import argparse
    # 命令行解析模块
    parser = argparse.ArgumentParser(description='Process some ...')
    
    # 输入参数filename
    parser.add_argument('-file')
    # 输入参数加密or解密
    parser.add_argument('-ende')

    args = parser.parse_args()
    print("所有文件均在当前目录生成...")
     
    if args.ende == "en":
        print("enenen")
        encryption(args.file)

    elif args.ende == "de":
        print("dedede")
        decryption(args.file)

"""
所有文件均在当前目录生成
加密时：en-decryption.exe -file 需要被加密的文件名 -ende en
解密时：en-decryption.exe -file 需要被解密的文件名 -ende de
"""