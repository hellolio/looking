# -*- coding: utf-8 -*-  
# Date：         2019/9/8  下午1:28
# Description：  SQL语句的相关操作，建立数据库连接由create_db_conn装饰器完成
import functools
import threading

from sqlalchemy.engine import Engine, Connection

from api_db.db_config import db
from api_db.db_models import Baidunew, User, Blog
from api_db.logging_config import logger


class Db_handle_by_sql(object):
    __instance = False
    rlock = threading.RLock()

    def __new__(cls, *args, **kwargs):
        with Db_handle_by_sql.rlock:
            if cls.__instance:
                return cls.__instance
            cls.__instance = super(Db_handle_by_sql, cls).__new__(cls, )
            return cls.__instance

    def create_db_conn(self,func):
        @functools.wraps(func)
        def wrapper(self, *args, **kw):
            conn = None
            engine = db.ENGINE
            if isinstance(engine, Engine):
                conn = engine.connect()
            result = []
            if isinstance(conn, Connection):
                try:
                    result = func(self, conn, *args, **kw)
                    conn.close()
                except Exception as e:
                    logger.ERROR.error(f"与数据库建立链接失败，{e}")
            return result
        return wrapper

    @create_db_conn
    def create_table(self, conn, table_name):
        try:
            sql = f'''CREATE TABLE IF NOT EXISTS `{table_name}`(
                            `id` INT AUTO_INCREMENT,
                            `title` VARCHAR(100) NOT NULL,
                            `author` VARCHAR(40) NOT NULL,
                            `date` varchar(10),
                            `is_delete` boolean not null default 0,
                            PRIMARY KEY (`id`)
                    )ENGINE=InnoDB DEFAULT CHARSET=utf8;'''
            conn.execute(sql)
            return True
        except Exception as e:
            logger.SQL.error(f"创建数据库错误,{e}")
        return False

    @create_db_conn
    def insert_to_sql(self, conn,title,author,date):
        try:
            sql = f"insert into baidunews (title,author,date) values ('{title}','{author}','{date}');"
            conn.execute(sql)
            return True
        except Exception as e:
            logger.SQL.error(f'插入数据错误，{e}')
            return False

    @create_db_conn
    def find_sql(self,conn):
        try:
            sql = f"select * from baidunews;"
            result = conn.execute(sql).fetchall()
            return result
        except Exception as e:
            logger.SQL.error(f'插入数据错误，{e}')
            return None

    @create_db_conn
    def delete_sql(self, conn, min_id, max_id):
        try:
            sql = f"delete from t_request_statistics where id between {min_id} and {max_id}"
            conn.execute(sql)
            return True
        except Exception as e:
            logger.SQL.error(f"删除数据错误,{e}")
        return False

    @create_db_conn
    def update_sql(self,conn,value):
        try:
            sql = f"update baidunews set title={value} where id=1;"
            conn.execute(sql)
            return True
        except Exception as e:
            logger.SQL.error(f'更新数据出错,{e}')
            return False


class Db_handle_by_session(object):
    __instance = False
    rlock = threading.RLock()

    def __new__(cls, *args, **kwargs):
        with Db_handle_by_session.rlock:
            if cls.__instance:
                return cls.__instance
            cls.__instance = super(Db_handle_by_session, cls).__new__(cls, )
            return cls.__instance

    def __init__(self):
        self.Session = db.get_session()

    # 查询数据
    def find_sql(self):
        session = self.Session()
        data = session.query(Baidunew).all()
        session.close()
        return data

    def insert_to_sql(self,title,author,date,url=''):
        session = self.Session()
        baidunews = Baidunew(title=title,author=author,date=date,url=url,is_delete=0)
        session.add(baidunews)
        session.commit()
        session.close()

    # 更新
    def update_sql(self):
        session = db.get_session()
        user_result = session.query(User).filter_by(id='1').first()
        user_result.name = "jack"
        session.commit()
        session.close()

    # 删除
    def delete_sql(self):
        session = self.Session()
        user_willdel = session.query(User).filter_by(id='5').first()
        session.delete(user_willdel)
        session.commit()
        session.close()

    # 新增数据
    def commint_sql(self):
        session = self.Session()
        new_user = User(id=2, username='haha', password='liaojun', email='3@163.com')
        new_blog = Blog(id=1, title='ww', text='文章正文内容', user_id=5)
        new_blog2 = Blog(id=4, title='haeei', text='文章正文内容', user_id=2)
        # session.add_all([new_user,new_blog2])  # 添加到session
        session.commit()  # 提交即保存到数据库
        session.close()  # 关闭session

    # pandas写入数据库
    def pandas_sql(self):
        d = [[1, 'hh', 'ff', 5]]
        import pandas as pd
        data = pd.DataFrame(d, columns=['id', 'username', 'password', 'email'])
        print(data)
        # chunksize可以设置一次入库的大小；if_exists设置如果数据库中存在同名表怎么办，
        # ‘replace’表示将表原来数据删除放入当前数据；‘append’表示追加；
        # ‘fail’则表示将抛出异常，结束操作，默认是‘fail’；
        # index=接受boolean值，表示是否将DataFrame的index也作为表的列存储
        data.to_sql('user', con=db.get_engine, chunksize=1000, if_exists='append', index=None)


