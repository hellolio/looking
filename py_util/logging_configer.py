# -*- coding: utf-8 -*-  
# Date：         2019/9/8  下午1:12
# Description：  日志记录（info,error,io,sql,redis,api），实列被导入后本身就是单列，
import os
import threading
import time
import logging.config

BASE_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'logs')

BASE_LOG_DIR = ['info','error','sql','redis','api']
if not os.path.isdir(BASE_DIR):
    os.mkdir(BASE_DIR)

for i in BASE_LOG_DIR:
    log_dir = os.path.join(BASE_DIR,i)
    if not os.path.isdir(log_dir):
        os.mkdir(log_dir)

DATE = time.strftime("%Y-%m-%d", time.localtime(time.time()))

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] - [%(threadName)s:%(thread)d] - [%(filename)s:%(lineno)d]'
                      ' - %(levelname)s - %(message)s'
        },
        'simple': {
            'format': '[%(asctime)s] - [%(filename)s:%(lineno)d] - %(levelname)s - %(message)s'
        }
    },
    'filters': {},
    'handlers': {
        # 打印到终端的日志
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        # 打印到文件的日志,收集info及以上的日志
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 保存到文件，按日期切割
            'filename': os.path.join(os.path.join(BASE_DIR,'info'), f"info_{DATE}.log"),  # 日志文件
            'backupCount': 7,
            'interval': 1,
            'when': 'MIDNIGHT',
            'formatter': 'simple',
            'encoding': 'utf-8',
        },
        # 打印到文件的日志:收集错误及以上的日志
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 保存到文件，按日期切割
            'filename': os.path.join(os.path.join(BASE_DIR,'error'), f"err_{DATE}.log"),  # 日志文件
            'backupCount': 7,
            'interval': 1,
            'when': 'MIDNIGHT',
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 打印到文件的日志:收集数据库日志
        'sql': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 保存到文件，按日期切割
            'filename': os.path.join(os.path.join(BASE_DIR,'sql'), f"sql_{DATE}.log"),  # 日志文件
            'backupCount': 7,
            'interval': 1,
            'when': 'MIDNIGHT',
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 打印到文件的日志:收集redis日志
        'redis': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 保存到文件，按日期切割
            'filename': os.path.join(os.path.join(BASE_DIR,'redis'), f"redis_{DATE}.log"),  # 日志文件
            'backupCount': 7,
            'interval': 1,
            'when': 'MIDNIGHT',
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        # 打印到文件的日志:收集api日志
        'api': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',  # 保存到文件，按日期切割
            'filename': os.path.join(os.path.join(BASE_DIR,'api'), f"api_{DATE}.log"),  # 日志文件
            'backupCount': 7,
            'interval': 1,
            'when': 'MIDNIGHT',
            'formatter': 'standard',
            'encoding': 'utf-8',
        }
    },
    'loggers': {
        'info': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'error': {
            'handlers': ['console', 'error'],
            'level': 'ERROR',
        },
        'sql': {
            'handlers': ['sql'],
            'level': 'INFO',
        },
        'redis': {
            'handlers': ['redis'],
            'level': 'INFO',
        },
        'api': {
            'handlers': ['api'],
            'level': 'INFO',
        }
    },
}


class Logger(object):
    __instance = False
    rlock = threading.RLock()

    def __new__(cls, *args, **kwargs):
        with Logger.rlock:
            if cls.__instance:
                return cls.__instance
            cls.__instance = super(Logger, cls).__new__(cls, )
            return cls.__instance

    def __init__(self):
        logging.config.dictConfig(LOGGING)
        self.INFO = logging.getLogger('info')
        self.ERROR = logging.getLogger('error')
        self.SQL = logging.getLogger('sql')
        self.REDIS = logging.getLogger('redis')
        self.API = logging.getLogger('api')


logger = Logger()
