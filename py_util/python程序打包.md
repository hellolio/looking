- 安装 pip install pyinstaller

- 被带包代码

(```)

    # aa.py
    import pandas as pd
    def cre_df(args):
        df1 = pd.DataFrame([["2020-01-01",args[1],"1919-01-01"],["2020-01-01","bb","1919-01-01"]],columns=["ymd","aa", "date"])
        list1 = ["ymd", "date"]
        df1[list1] = df1[list1].apply(lambda x:pd.to_datetime(x, format='%Y-%m-%d'))
        df1.to_csv(args[0])
        return df1

    if __name__=="__main__":
        import argparse
        # 命令行解析模块
        parser = argparse.ArgumentParser(description='Process some integers.')
        
        # 输入参数为字符串，共两个，跟在-i后面，传到args.i中
        parser.add_argument('-i', type=str, nargs=2, help='bar help')

        # 只需输入-check，后面不需要跟参数，自动执行action的store_const，这个指向了const=True，所以返回True，这里const=sum也行，表示返回一个函数，加()后可直接调用这个函数
        parser.add_argument('-check', action="store_const", const=True, required=False)

        args = parser.parse_args()

        cre_df(args.i)


(```)

- 打包命令 `pyinstaller -F -w aa.py`
- -w 表示打开.exe程序时不生成控制台(程序中有print时注意)，-F表示打包成单个可执行文件
- 执行(在没有python环境的电脑中也可执行):.\aa.exe -i rr.csv hh -o hdh -check
- 请参考当前目录下的en-decryption.py(加解密程序)