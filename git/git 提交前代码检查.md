将下面的代码写入一个叫`.git/hooks/pre-commit`的文件中就可以了

```
#!/bin/sh

# 运行 flake8 检查
flake8 .

# 如果 flake8 检查失败，退出
if [ $? -ne 0 ]; then
    echo "Code style checks failed. Commit aborted."
    exit 1
fi
```

还有很多其他规则，比如`commit-msg`可以检查message的格式
```
#!/bin/sh

commit_msg_file=$1
commit_msg=$(cat "$commit_msg_file")

# 检查提交消息是否符合格式
if ! echo "$commit_msg" | grep -qE '^(JIRA-\d+): .+'; then
    echo "提交消息格式不正确！应为 'JIRA-123: 描述信息'"
    exit 1
fi
```