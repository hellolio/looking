git log

git rebase -i HEAD~3  -i 的参数是不需要合并的 commit 的 hash 值，这里的10b73908 为 d8915ad2 的前一次提交记录；
该命令执行后，进入 vi 的编辑模式，6次提交的commit倒序排列，最下面的是最近的一次提交记录。


修改第2~6行的第一个单词 pick 为 squash or s，然后 输入：wq or x 保存退出。
pick 的意思是要执行这个 commit
squash 的意思是这个 commit 会被合并到前一个 commit

git 会压缩提交历史，若有冲突，需要进行修改，修改的时候保留最新的历史记录，修改完之后输入以下命令：
git add .
git rebase --continue
若想退出放弃此次压缩，执行命令：
git rebase --abort
若无冲突 or 冲突已 fix，则会出现一个 commit message 编辑页面，修改 commit message ，然后 输入：wq or x 保存退出。


3、同步到远程 git 仓库
输入：git push -f or git push --force
查看远程仓库效果，多次 commit 已被合并成一次 commit。

