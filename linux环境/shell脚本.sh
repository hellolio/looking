echo "===================================================================="
echo "===sh 本shell.sh 输入参数.lst======================================="
echo "===按顺序执行输入参数中的命令（按行执行），遇到错误就停止==========="
echo "===================================================================="

read -p "是否继续，继续请输入y:" is_opt

if [ $is_opt != "y" ];then
    echo "未执行任何操作"
    exit 9
fi

while read -r line
do
    echo $line
    eval $line
    if [ $? -eq 0 ];then
        echo "commend 执行成功"
    else
        echo "commend 执行失败"
        break
    fi
done < $1



# 按顺序执行commends.lst文件中的命令（按行执行，每行一条命令），自动输入y
yes y|sh commends.lst



# 临时显示vi中的行号
输入":set number"后按回车键
# 永久显示行号
vi ~/.vimrc
在打开的vimrc文件中最后一行输入：set number
