# source /etc/profile.d/myenv.sh


# set miniconda environment
export PATH=/opt/miniconda3/bin:$PATH

# set openjdk environment
export JAVA_HOME=/opt/jdk-11
export PATH=$JAVA_HOME/bin:$PATH

# Git branch in prompt.
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}
export PS1="\e[32;1m[\u@\h \W\e[34;1m\$(parse_git_branch)\e[32;1m] $ "
